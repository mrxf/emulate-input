;(function() {
    window.currentSelectEmInput = null;
    const emInputs = document.querySelectorAll("em-input");
    console.log(emInputs);
    if(emInputs.length > 0) {
        window.currentSelectEmInput = emInputs[0];
    }
    emInputs.forEach(v => {
        const valueContainer = document.createElement('value');
        valueContainer.addEventListener('select', function(e) {
            console.log(e);
        }, true);
        v.appendChild(valueContainer);
        v.addEventListener('click', function() {
            window.currentSelectEmInput = v;
            emInputs.forEach(item => {
                item.classList.remove('focus');
            })
            v.classList.add('focus');
        })
    });

    document.addEventListener('keydown', function(e) {
        if(window.currentSelectEmInput) {
            window.currentSelectEmInput.firstChild.innerText += e.key;
        }
    }, true);
    document.addEventListener('click', function(e) {
        if(window.currentSelectEmInput) {
            window.currentSelectEmInput.classList.remove('focus');
            window.currentSelectEmInput = null;
        }
    }, true);
})();