# 输入框模拟器

> 一款使用javascript模拟原生Input输入框的功能

## 问题来源

在一些特殊的浏览器中，例如`微信内置浏览器`，打开网页，如果页面中含有input框，会在头部提示`请勿在此输入个人信息`，因此可以使用这种模拟输入框来解决此类问题